# CALS Tables


*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Compare-10_0_0_j/samples/sample-name`.*

---
This document describes how to run the CALS Tables sample. For concept details see: [Comparing Document Tables (CALS or HTML)](https://docs.deltaxml.com/support/latest/documentation/comparing-document-tables-cals-or-html) and also its child page.
The input files contain a set of tables that replicate most of the examples given on the child page, [Examples of Table Comparison Results](https://docs.deltaxml.com/support/latest/documentation/comparing-document-tables-cals-or-html/examples-of-table-comparison-results). The example numbers from the web page are given as the title of each table.
The examples 15 to 18 only appear here, in Bitbucket, and show the advantages of using column names.

In this sample the DCP file is used to configure the comparator pipeline.

When using any Java version of XML Compare, the commands to run the CALS table sample configuration are as follows. Replace "x.y.z" with the major.minor.patch version number of your release e.g. `command-10.0.0.jar`

	java -jar ../../command-x.y.z.jar compare cals input1.xml input2.xml result.html
	java -jar ../../command-x.y.z.jar compare cals input1.xml input2.xml result.xml oxygen-output=true

The first line above produces a HTML file that shows the differences between the tables.  This uses styling that is provided "as is" for convenience in this sample and should not be used in a production environment. The HTML file can be displayed in a browser.

The second line produces an XML file with Oxygen "track changes" instructions that can be opened in the [Oxygen XML Editor](https://www.oxygenxml.com). When viewed in Author mode the tables are rendered with the changes marked.