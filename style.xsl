<?xml version="1.0" encoding="UTF-8"?>
<!-- 
       Converts DeltaV2 output specifically to convert CALS tables to HTML tables with differences highlighted.
       This XSLT is not intended for any input files, just those included in the CALS table processing sample.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1"
  exclude-result-prefixes="#all"
  version="3.0">
  
  <xsl:mode on-no-match="shallow-copy"/>
  <xsl:output method="html" version="5"/>
  
  <xsl:param name="include-css" as="xs:boolean" select="true()"/>
  
  <xsl:template match="/">
    <html>
      <head>
        <xsl:choose>
          <xsl:when test="$include-css">
            <style>
              <xsl:sequence select="unparsed-text(resolve-uri('htmltable.css', static-base-uri()))"/>
            </style>
          </xsl:when>
          <xsl:otherwise>
            <link rel="stylesheet" type="text/css" href="htmltable.css"/>
          </xsl:otherwise>
        </xsl:choose>

        
        <xsl:apply-templates select="topic/title"/>
      </head>
      <xsl:apply-templates select="topic/body"/>
    </html>
  </xsl:template>
    
  <xsl:template match="*:topic|*:tgroup">
      <xsl:apply-templates select="node()"/>    
  </xsl:template>
  
  <xsl:template match="*:tbody|*:thead|*:tfoot">
    <xsl:element name="{local-name(.)}">
      <xsl:apply-templates select="parent::tgroup/@*"/>
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>
     
  
  <xsl:template match="*:row">
    <tr>
      <xsl:apply-templates select="@*, node()"/>
    </tr>
  </xsl:template>
  
  <xsl:template match="*:entry">
    <td>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="node()"/>
    </td>
  </xsl:template>
  
  <xsl:template match="*:entry[@nameend][@namest]">
    <td>
      <xsl:apply-templates select="@*"/>
      <xsl:attribute name="colspan" select="deltaxml:calcColspan(.)"/>      
      <xsl:apply-templates select="node()"/>
    </td>
  </xsl:template>
  
  <xsl:template match="deltaxml:textGroup">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="deltaxml:text">
    <span>
      <xsl:apply-templates select="@*, node()"/>
    </span>
  </xsl:template>
  
  <xsl:template match="*:table/*:title">
    <caption>
      <xsl:apply-templates/>
    </caption>
  </xsl:template>
  
  <xsl:template match="*:p[*:table]">
    <section>
      <xsl:apply-templates/>
    </section>
  </xsl:template>
  
  <xsl:template match="@morerows">
    <xsl:attribute name="rowspan" select="xs:integer(.) + 1"/>
  </xsl:template>
  
  <xsl:template match="@deltaxml:deltaV2">
    <xsl:attribute name="data-deltaV2" select="."/>
  </xsl:template>
  
  <xsl:template match="@colname|@frame|@rowsep|@colsep"/>
  
  <xsl:template match="*:colspec"/>
  
  <xsl:function name="deltaxml:calcColspan" as="xs:integer">
    <xsl:param name="elem" as="element()"/> <!-- an entry, entrytbl or spanspec -->
    <xsl:variable name="namest" as="attribute()" select="$elem/@namest"/>
    <xsl:variable name="nameend" as="attribute()" select="$elem/@nameend"/>
    
    <xsl:variable name="namestColnum" as="xs:integer" 
                  select="xs:integer($elem/ancestor::*:tgroup[1]/*:colspec[@colname=$namest]/@colnum)"/>
    <xsl:variable name="nameendColnum" as="xs:integer" 
      select="xs:integer($elem/ancestor::*:tgroup[1]/*:colspec[@colname=$nameend]/@colnum)"/>
    
    <xsl:sequence select="($nameendColnum - $namestColnum) + 1"/>

  </xsl:function>
  
</xsl:stylesheet>